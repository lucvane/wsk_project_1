# Dialer - project 1
Web app that allows users to making phone calls through browser.

### Prerequisites
* [Node.js (v8.x)](https://nodejs.org/en/)
* [npm (v5.x)](https://www.npmjs.com/)
---

## Installation

Open terminal and go to your project folder using "cd" command. 
For example:
```bash
cd C:\WSK_Project_1
```
Before running app, set login and password in dialer.js file:
```javascript
const config = {
 url: 'https://uni-call.fcc-online.pl',
 login: 'PUT LOGIN HERE',
 password: 'PUT PASSWORD HERE'
};
```
If your project path is correct then run following comand in terminal:
```bash
npm install
node dialer.js
```
If app is running correctly, the Comand Interpreter shoudl display:
```
app listening on port 3000
``` 
---
## Making calls through browser.
For example, you can use [restninja](https://restninja.io/) website.

On the top-right corner set "ajax".

Switch request type from GET to POST and set request uri to:
```
http://localhost:3000/call/
```

Set "body" section to: 

```
{
 "number1": "xxx",
 "number2": "xxx2"
}
```
Where "xxx" and "xxx2" are yours number of choice. If you want call yourself, set one of the number to "999999999".

Switch from "body" section to "header" section and set

Header to:
```
content-type
```
Value to:
```
application/json
```

Click button "Send" and if everything is setting correctly, you shoudl get response:

```
{
  "success": true,
  "id": 0
}
```

If you want check current status of any calls that have been made, go to:

```
http://localhost:3000/status/0
```

Each call will have its own specific "id", 0 is only for example.

You can also check status of current call on [socketio-client-tool](https://amritb.github.io/socketio-client-tool/).

Set "Socket.io server URL" to:
```
http://localhost:3000/
```
and on "Listening" page set "Event name" to:
```
status
```
then click "Listen" button.