const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const config = {
 url: 'https://uni-call.fcc-online.pl',
 login: 'PUT LOGIN HERE',
 password: 'PUT PASSWORD HERE'
};

Dialer.configure(config);
app.use(cors());
app.use(bodyParser.json());

http.listen(3000, () => {
    console.log('app listening on port 3000');
   });

io.on('connection', (socket) => {
    console.log('a user connected')
    socket.on('disconnect', () => {
     console.log('a user disconnected')
    })
    socket.on('message', (message) => {
            console.log('message', message)
        })
        socket.on('status', (status) => {
            console.log('status', status)
        })
        io.emit('message', 'connected!')     
   })
   
let bridge = null;
let currentStatus = null;
const arrayIdStatus = [];
let countId = 0;
let currentId = null;

app.post('/call/', async (req, res) => {
    currentId = countId;
    const body = req.body;
    console.log('id', currentId);
    console.log('body',body);
    try {
        bridge = await Dialer.call(body.number1, body.number2);
    } catch (error) {
        res.json({error: "Take some time before making another call"});
        return;
    }   
    arrayIdStatus.push({
        id: currentId,
        status: currentStatus
    });
    countId++; 
    let interval = setInterval(async () => {
        let status = await bridge.getStatus();
        if(currentStatus !== status) {
            currentStatus = status;
            io.emit('status', status);
            console.log(status);
        }
        if(currentStatus === "ANSWERED" || currentStatus === "FAILED"
        || currentStatus === "BUSY" || currentStatus === "NO ANSWER") {
            console.log("Call end");
            clearInterval(interval);
        }
    },500)
     res.json( {success: true , id: currentId});
    })

app.get('/status/:callsId', async (req, res) => { 
    const callsId = req.params.callsId;   
    const requestedCallsId = arrayIdStatus.find( item => item.id == callsId );
    const requestedCallsIdIndex = arrayIdStatus.indexOf(requestedCallsId);
    if(bridge !== null && arrayIdStatus[requestedCallsIdIndex] !== undefined && arrayIdStatus[requestedCallsIdIndex] !== null) {
        if(requestedCallsId!== undefined && requestedCallsId !== null){
            const requestedCurrentId = arrayIdStatus.find( item => item.id == currentId );
            const requestedCurrentIdIndex = arrayIdStatus.indexOf(requestedCurrentId);
            arrayIdStatus[requestedCurrentIdIndex] = {
                id: currentId,
                status: currentStatus
        };
        res.json(arrayIdStatus[requestedCallsIdIndex]);
        }else {
            res.json({success: false , id: "id not exist"});
        }
    } else {
        res.json({success: false , id: "id not exist"});
    }  
});
